# Lab1

## Problem 3

* Ballerina comes with a set of command line interfaces presented here. Choose the appropriate commands to create a project.
You will change the content of the main file to display your student and course details (student number, fullname, course code
and designation).
Add the git protocol to your project and deploy it to Gitlab.

## Problem 4

* Create a project that randomly selects a number between 10 and 60 and then compute the factors of the selected number. The
program should print out the selected random number as well as all its factors. Deploy your project to Gitlab.