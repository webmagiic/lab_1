import ballerina/io;
import ballerina/random;

public function main() returns error? {

    //Problem 3
    io:println("Student Number: 219036810\nFull Name: Braulio Andre\nCourse Code: DSA612S2\nDesignation: Bachelor of Computer Science");

    //Problem 4
    int randomInteger = check random:createIntInRange(10, 60);

    int factor = 1;

    io:println("Chose Integer: ", randomInteger, "\nFactors:");

    while factor <= randomInteger {
        if randomInteger % factor == 0 {
            io:println(factor);
        }
        factor += 1;
    }

}
